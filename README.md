# Summary of the Final Report

## **Introduction**
The pilot project, "Optimizing Global Vegetation Models with a Model-Data Integration (MDI) Approach," aimed to address the uncertainties in terrestrial ecosystem responses to climate change, a critical component of the global carbon cycle. Dynamic Global Vegetation Models (DGVMs), such as LPJmL (Lund-Potsdam-Jena managed Land), are used to simulate these processes, but biases in parameterization can lead to inaccuracies. The pilot focused on developing an MDI framework to integrate observational data into DGVMs, improving their performance in modeling vegetation phenology, productivity, and carbon fluxes.

The project was conducted by **Technische Universität Dresden**, led by **Naixin Fan** and **Matthias Forkel**, and funded by the German Research Foundation under the NFDI4Earth initiative (DFG project no. 460036893).

## **Objectives**
The pilot aimed to:
1. Develop a model-data integration framework for optimizing DGVM parameters using observational data.
2. Demonstrate the framework's capability through a case study using the LPJmL model.
3. Provide an open-access R package for the MDI framework to facilitate broader community use and adaptation.

## **Key Features**
1. **Framework Implementation**:
   - Developed an R package for MDI that incorporates observational datasets to iteratively optimize model parameters using a genetic optimization algorithm (GENOUD).
   - Improved parameterization of LPJmL using site-level carbon flux data (e.g., GPP, Reco, NEE) and remote-sensed ecological variables (e.g., FAPAR).

2. **Optimization Results**:
   - Substantial reductions in the cost function for carbon fluxes (e.g., 90% for GPP, 92% for Reco), leading to more realistic simulations of ecological processes.

3. **Data Accessibility**:
   - The R package and related documentation are available under an AGPL-3.0 license on GitHub (repository link: [NFDI_MDI](https://github.com/fanfan987/NFDI_MDI.git)).

4. **Innovation and FAIRness**:
   - The framework demonstrates the feasibility of integrating observational data into DGVMs in a structured and transparent manner, aligning with FAIR principles.

## **Outcomes**
1. **Proven Concept**:
   - Successfully optimized the LPJmL model, reducing biases and uncertainties in its simulations.
   - Demonstrated the framework's potential applicability to other DGVMs and studies.

2. **Community Relevance**:
   - The MDI framework offers a valuable tool for Earth system science, atmospheric science, and ecological studies, enabling researchers to improve model accuracy using observational data.

3. **Challenges**:
   - Time limitations and preliminary developments within NFDI4Earth constrained the framework's generalization to other models.

## **Future Directions**
1. **Framework Generalization**:
   - Expand the MDI framework to support other DGVMs by adapting data input protocols and interfaces.
   - Link the MDI framework to data repositories or data cubes to streamline data preprocessing.

2. **Community Engagement**:
   - Provide guidance and support to researchers interested in adapting the framework for their models.

3. **Extended Research**:
   - Further refine the framework's algorithms and expand its functionality for broader applications in Earth system science.

The pilot project underscores the potential of the MDI framework to enhance the accuracy and usability of DGVMs, offering a robust foundation for future advancements in modeling terrestrial ecosystems and their responses to climate change.
